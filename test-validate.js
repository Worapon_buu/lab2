const chai = require('chai');
const expect = chai.expect;
const validate = require('./validate');

describe('Validate Module', ()=>{
    context('Function isUserNameValid', ()=>{
        it('Function prototype : boolean isUserNameValid(username: String)', ()=>{
            expect(validate.isUserNameValid('kob')).to.be.true;
        });

        it('จำนวนตัวอักษรอย่างน้อย 3 ตัวอักษร', ()=>{
            expect(validate.isUserNameValid('tu')).to.be.false;
        });

        it('ทุกตัวต้องเป็นตัวเล็ก', ()=>{
            expect(validate.isUserNameValid('Kob')).to.be.false;
            expect(validate.isUserNameValid('koB')).to.be.false;
        });

        it('จำนวนตัวอักษรที่มากที่สุดคือ 15 ตัวอักษร', ()=>{
            expect(validate.isUserNameValid('kob123456789012')).to.be.true;
            expect(validate.isUserNameValid('kob1234567890123')).to.be.false;
        });
    });
    
    context('Function isAgeValid',()=>{
        it('Function prototype : boolean isAgeValid (age: String)',()=> {
            expect(validate.isAgeValid('18')).to.be.true;
        });
        it('age ต้องเป็นข้อความที่เป็นตัวเลข',()=>{
           expect(validate.isAgeValid('a')).to.be.false;
       });
       it('อายุต้องไม่ต่ำกว่า 18 ปี และไม่เกิน 100 ปี', ()=>{
           expect(validate.isAgeValid('17')).to.be.false;
           expect(validate.isAgeValid('18')).to.be.true;
           expect(validate.isAgeValid('100')).to.be.true;
           expect(validate.isAgeValid('101')).to.be.false;
       });
    });

    context('Function isPasswordValid',()=>{
        it('Function prototype : boolean isUserNameValid(password: String)',()=> {
            expect(validate.isPasswordValid('A12345678!')).to.be.true;
        });
        it('จำนวนตัวอักษรอย่างน้อย 8 ตัวอักษร',()=>{
            expect(validate.isPasswordValid('1234567')).to.be.false;
        });
        it('ต้องมีอักษรตัวใหญ่เป็นส่วนประกอบอย่างน้อย 1 ตัว',()=>{
            expect(validate.isPasswordValid('A12345678!')).to.be.true;
            expect(validate.isPasswordValid('a12345678!')).to.be.false;
        });
        it('ต้องมีตัวเลขเป็นส่วนประกอบอย่างน้อย 3 ตัว',()=>{
            expect(validate.isPasswordValid('Abcdefg12!')).to.be.false;
        });
        it('ต้องมีอักขระพิเศษอย่างน้อย1ตัว',()=>{
            expect(validate.isPasswordValid('Abcdefg123!')).to.be.true;
            expect(validate.isPasswordValid('Abcdefg123')).to.be.false;
        });
    });

    context('function isDateValid',()=>{
        it('Function prototype : boolean isDateValid(day: Integer, month: Integer, year: Integer)', ()=> {
            expect(validate.isDateValid('12/02/2019')).to.be.true;
            expect(validate.isDateValid('aa/bb/cc')).to.be.false;
        });
        it('day เริ่ม 1 และไม่เกิน 31 ในทุก ๆ เดือน', ()=>{ 
            expect(validate.isDateValid('32/02/2019')).to.be.false;
        });
        it('month เริ่มจาก 1 และไม่เกิน 12 ในทุก ๆ เดือน', ()=> {
            expect(validate.isDateValid('15/15/2019')).to.be.false;
            expect(validate.isDateValid('15/0/2019')).to.be.false;
        });
        it('year จะต้องไม่ต่ำกว่า 1970 และ ไม่เกิน ปี 2020', ()=> {
            expect(validate.isDateValid('12/02/1950')).to.be.false;
            expect(validate.isDateValid('15/15/2021')).to.be.false;
        });
        it('เดือนแต่ละเดือนมีจำนวนวันต่างกันตามรายการ',()=> {
            expect(validate.isDateValid('29/02/2019')).to.be.false;
            expect(validate.isDateValid('30/02/2000')).to.be.false;
            expect(validate.isDateValid('31/04/2000')).to.be.false;
            expect(validate.isDateValid('31/06/2000')).to.be.false;
            expect(validate.isDateValid('31/09/2000')).to.be.false;
            expect(validate.isDateValid('31/11/2000')).to.be.false;
            expect(validate.isDateValid('31/12/2000')).to.be.true;
        });
        

    });
});