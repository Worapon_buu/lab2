module.exports = {
    isUserNameValid: function(username) {
        if(username.length<3 || username.length>15){
            return false;
        }
        //Kob -> kob
        if(username.toLowerCase() !== username){
            return false;
        }
        return true;
    },

    isAgeValid: function(age) {
        if(isNaN(age)){
            return false;
        }
        if(age<18 || age>100){
            return false;
        }
       return true;
    },

    isPasswordValid: function(password) {
       // let regex = '/^(?=.*[A-Z])(?=.*[0-9])(?=.*[#?!@$%^&*-]).{8,}$/'
        const uppercase = /[A-Z]/
        const three = /[0-9]{3}/
        const special =/[#?!@$%^&*-]/

        if(password.length<8){
            return false;
        }
        if(!uppercase.test(password)){
            return false ;
        }
        if(!three.test(password)){
            return false ;
        }
        if(!special.test(password)){
            return false;
        }
        return true;
    },

    isDateValid: function(date){
        const part = date.split('/');
        const day = parseInt(part[0],10);
        const month = parseInt(part[1],10);
        const year = parseInt(part[2],10);
        
        if(!day === parseInt(day)) {
            return false;
        }
        if(!(day>0 && day<32)){
            return false ;
        }
        if(!(month>0 && month<13)){
            return false;
        }
        if(!(year>1969 && year<2021)){
            return false;
        }
        
        //console.log(year%400==0)
        //console.log(year)

        switch(month){          
            case 2: if(year%400==0){
                        if(day>29) return false;
                    }else{
                        if(day>28) return false;
                    } ;        
                break;
            case 4: if(day>30) return false;
                break;
            case 6: if(day>30) return false;
                break;
            case 9: if(day>30) return false;
                break;
            case 11: if(day>30) return false;
                break;
        }
        return true;
    }


}